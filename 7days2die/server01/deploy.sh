#!/usr/bin/env bash

distrobox create \
--name "7days2die-server01" \
--image "ubuntu:latest" \
--pre-init-hooks "dpkg --add-architecture i386; apt update" \
--additional-packages "curl wget file tar bzip2 gzip unzip bsdmainutils python3 util-linux ca-certificates binutils bc jq tmux netcat lib32gcc-s1 lib32stdc++6 libsdl2-2.0-0:i386 steamcmd telnet expect libxml2-utils" \
--home "$HOME/containers/7days2die/server01/home" \
--init-hooks "curl -Lo linuxgsm.sh https://linuxgsm.sh && chmod +x linuxgsm.sh && bash linuxgsm.sh sdtdserver"
